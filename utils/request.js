// const baseUrl = 'http://kr-nc-bgp-1.openfrp.top:45678/api';
// const baseUrl = 'http://39.103.194.0:11028/api';
const baseUrl = 'https://miniprogram.app-cssasheffield.com/api'

function request(option) {
	let data = {
		...option.data,
	}
	return new Promise((resolve, reject) => {
		uni.request({
			method: option.method,
			url: baseUrl + option.url + '?token=' + uni.getStorageSync('token'),
			data: data || {},
			header: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).then(async (response) => {
			if (response.data.code == 201) {
				// await this.$onLaunched
				// uni.clearStorage();
				// uni.showModal({
				// 	title: '未登录',
				// 	content: '为了方便您更好体验该小程序，请前往我的页面登录',
				// 	success(res) {
				// 		if(res.confirm) {
				// 			uni.switchTab({
				// 				url: '/pages/my/my'
				// 			})
				// 		}
				// 	}
				// })
				
				uni.showToast({
					icon: 'none',
					title: '当前状态为未登录，请登录之后查看相关内容！'
				})

				setTimeout(() => {
					uni.switchTab({
						url: '/pages/my/my'
					})
				}, 1000)
				resolve(response.data);
			} else if (response.data.code == 200) {
				resolve(response.data);
			} else {
				resolve(response.data);
			}
		}).catch(error => {
			let [err, res] = error;
			reject(err)
		})
	});
}

export default request
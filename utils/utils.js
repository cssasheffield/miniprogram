export const groupList = (array, group) => {
	let index = 0;
	let newArray = [];
	while (index < array.length) {
		newArray.push(array.slice(index, (index += group)));
	}
	return newArray;
}

export const rowList = (array) => {
	let newArray = [];
	let isArray = false
	for (let i = 0; i < array.length; i++) {
		if (array[i].length > 0) {
			isArray = true
			break;
		}
	}
	if (isArray) {
		array.map(item => {
			item.map(v => {
				newArray.push(v)
			})
		})
		return newArray
	} else {
		return array
	}
}


export const getArrayIndex = (arr, strId) => {
	let i = arr.length;
	while (i--) {
		if (arr[i].id === strId) {
			return i;
		}
	}
	return -1;
}

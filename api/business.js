import request from '@/utils/request.js'

const urlName = '/rp/business/'

export function getBusinessList(data) {
	return request({
		url: `${urlName}list`,
		method: 'get',
		data
	})
}

export function getBusinessInfo(data) {
	return request({
		url: `${urlName}info`,
		method: 'get',
		data
	})
}

export function submitBusinessAppraiseSave(data) {
	return request({
		url: `${urlName}appraise/save`,
		method: 'post',
		data
	})
}

export function getBusinessAppraiseList(data) {
	return request({
		url: `${urlName}appraise/list`,
		method: 'get',
		data
	})
}
import request from '@/utils/request.js'

const urlName = '/rp/redeemRecord'

export function getRedeemRecordMyList(data) {
	return request({
		url: `/${urlName}/myList`,
		method: 'get',
		data
	})
}

export function submitRedeemRecordSave(data) {
	return request({
		url: `/${urlName}/save`,
		method: 'POST',
		data
	})
}
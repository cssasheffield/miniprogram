import request from '@/utils/request.js'

const urlName = '/rp/goods'

export function getGoodsInfo(data) { // 详情
	return request({
		url: `/${urlName}/info`,
		method: 'get',
		data
	})
}

export function getGoodsList(data) { // 列表
	return request({
		url: `/${urlName}/list`,
		method: 'get',
		data
	})
}

export function getGoodsMyList(data) { // 我的物品列表
	return request({
		url: `/${urlName}/myList`,
		method: 'get',
		data
	})
}

export function submitGoodsSave(data) { // 保存物品信息
	return request({
		url: `/${urlName}/save`,
		method: 'post',
		data
	})
}

export function submitGoodsUpdateStatus(data) { // 上架/下架
	return request({
		url: `/${urlName}/updateStatus`,
		method: 'post',
		data
	})
}

export function getCategoryList(data) { // =分类数据
	return request({
		url: `/${urlName}/category/list`,
		method: 'get',
		data
	})
}
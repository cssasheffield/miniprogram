import request from '@/utils/request.js'

const urlName = '/member/'

export function memberRegister(data) {
	return request({
		url: `${urlName}/card/register`,
		method: 'post',
		data
	})
}

export function memberInfo(data) {
	return request({
		url: `${urlName}/info`,
		method: 'get',
		data
	})
}


export function memberCardPay(data) {
	return request({
		url: `${urlName}/card/pay`,
		method: 'post',
		data
	})
}

export function memberCardInfo(data) {
	return request({
		url: `${urlName}/card/info`,
		method: 'get',
		data
	})
}

export function memberCardRenew(data) {
	return request({
		url: `${urlName}/card/renew`,
		method: 'post',
		data
	})
}

export function memberCardBind(data) {
	return request({
		url: `${urlName}/card/bind`,
		method: 'post',
		data
	})
}

export function memberCardImageUpload(data) {
	return request({
		url: `${urlName}/card/imageUpload`,
		method: 'post',
		data
	})
}

export function memberUpdateInfo(data) {
	return request({
		url: `${urlName}/updateInfo`,
		method: 'post',
		data
	})
}
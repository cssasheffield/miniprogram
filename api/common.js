import request from '@/utils/request.js'

const urlName = '/common'

export function getDictList(data) {
	return request({
		url: `${urlName}/dictList`,
		method: 'get',
		data
	})
}

import request from '@/utils/request.js'

const urlName = 'wxMini'

export function wxLogin(data) {
	return request({
		url: `/${urlName}/login`,
		method: 'post',
		data
	})
}

export function wxRegister(data) {
	return request({
		url: `/${urlName}/register`,
		method: 'post',
		data
	})
}

export function getCompanyIntro(data) {
	return request({
		url: `/app/companyIntro/info`,
		method: 'get',
		data
	})
}
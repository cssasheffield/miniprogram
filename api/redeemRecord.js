import request from '@/utils/request.js'

const urlName = '/rp/redeemGoods'

export function getRedeemGoodsList(data) {
	return request({
		url: `/${urlName}/list`,
		method: 'get',
		data
	})
}


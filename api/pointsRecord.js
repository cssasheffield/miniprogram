import request from '@/utils/request.js'

const urlName = '/rp/pointsRecord'

export function getPointsRecordMyList(data) {
	return request({
		url: `/${urlName}/myList`,
		method: 'get',
		data
	})
}

export function submitPointsRecordSave(data) {
	return request({
		url: `/${urlName}/save`,
		method: 'POST',
		data
	})
}
import request from '@/utils/request.js'


export function getActivity(data) {
	return request({
		url: `/rp/activity/list`,
		method: 'get',
		data
	})
}

export function getNotice(data) {
	return request({
		url: `/app/notice/list`,
		method: 'get',
		data
	})
}

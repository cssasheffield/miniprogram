export const mixins = {
	data() {
		return {
			pageNo: 1,
			pageSize: 10,
			count: 0,
			status: 'loadmore',
		}
	},
	onReachBottom() {
		if (this.isNoOnReachBottom) return
		if (this.list && this.list.length >= this.count) return;
		this.status = 'loading';
		this.pageNo = ++this.pageNo;
		setTimeout(async () => {
			let resList = await this.getList(this.urlApi, this.params, {
				pageNo: this.pageNo,
				pageSize: this.pageSize
			})
			this.list = [...this.list, ...resList]
			if (this.list.length >= this.count) this.status = 'nomore';
			else this.status = 'loadmore';
		}, 1000)
	},
	methods: {
		async getList(urlApi, data) {
			if (typeof urlApi !== 'function') return;

			const res = await urlApi({
				...data,
				pageNo: this.pageNo,
				pageSize: this.pageSize,
			})
			this.count = res.data.count
			if (res.code === 202) {
				return res
			} else {
				if (res.data.list.length >= this.count) this.status = 'nomore';
				else this.status = 'loadmore';
				return res.data.list
			}
			
		},
		async inpuChange() {
			this.list = await this.getList(this.urlApi, this.params, {
				pageNo: 1,
				pageSize: 30
			});
		}
	}
}